# video-club

## Descripciòn
Proyecto para administrar un video-club utilizando express


## Requisitos
```
Javascript
Node.js
```

## Instalacion
Para clonar el repositorio(Se debe tener una llave ssh):
```
git@gitlab.com:ElGary3k/video-club.git
```
Se instalan las dependencias:
```
npm install
```
Se debe crear una base de datos en MySQL


## Autor
Angel Eduardo Garibay Valenzuela
