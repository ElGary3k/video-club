const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const acl = require('acl');
const Sequelize = require('sequelize');
const db = require('./db');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const directorsRouter = require('./routes/directors');
const genresRouter = require('./routes/genres');
const moviesRouter = require('./routes/movies');
const actorsRouter = require('./routes/actors');
const membersRouter = require('./routes/members');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

  //middlewares de enrutamiento
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/directors', directorsRouter);
app.use('/genres',genresRouter);
app.use('/movies',moviesRouter);
app.use('/actors',actorsRouter);
app.use('/members',membersRouter);
app.use('/copies',copiesRouter);
app.use('/bookings',bookingsRouter);


// Usar la base de datos con Sequelize
const sequelizeBackend = new acl.sequelizeBackend(db.sequelize, { prefix: 'acl_' });
const aclInstance = new acl(sequelizeBackend);


// Definir roles y permisos
aclInstance.allow([
  {
    roles: 'admin',
    allows: [
      { resources: '/movies', permissions: ['get', 'post', 'put', 'delete'] },
      { resources: '/actors', permissions: ['get', 'post', 'put', 'delete'] },
      { resources: '/directors', permissions: ['get', 'post', 'put', 'delete'] },
      // Otros permisos para admin
    ],
  },
  {
    roles: 'user',
    allows: [
      { resources: '/movies', permissions: ['get'] },
      { resources: '/actors', permissions: ['get'] },
      { resources: '/directors', permissions: ['get'] },
      // Otros permisos para user
    ],
  },
]);



// Middleware para verificar permisos
function aclMiddleware(role) {
  return function (req, res, next) {
    const resource = req.originalUrl;
    const method = req.method.toLowerCase();

    aclInstance.isAllowed(role, resource, method, function (err, allowed) {
      if (err) {
        res.status(500).send('Error en el control de acceso');
      } else if (allowed) {
        next();
      } else {
        res.status(403).send('No tienes permisos para acceder a este recurso');
      }
    });
  };
}

module.exports = { aclInstance, aclMiddleware };



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
