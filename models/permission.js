const { DataTypes, UUIDV4 } = require('sequelize');

module.exports = (sequelize) => {
  const Permission = sequelize.define('Permission', {
    id: {
      type: DataTypes.UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.ENUM('READ', 'CREATE', 'UPDATE', 'DELETE'),
      allowNull: false,
    },
  });

  return Permission;
};
