const express = require('express');
const router = express.Router();
const controller = require('../controllers/users');
const { aclMiddleware } = require('../acl');

router.post('/', aclMiddleware('admin'), controller.create);
router.get('/', aclMiddleware('user'), controller.list);
router.get('/:id', aclMiddleware('user'), controller.index);
router.put('/:id', aclMiddleware('admin'), controller.replace);
router.patch('/:id', aclMiddleware('admin'), controller.update);
router.delete('/:id', aclMiddleware('admin'), controller.destroy);

module.exports = router;
